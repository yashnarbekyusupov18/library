package uz.beepro.library.controller;

import org.springframework.web.bind.annotation.*;
import uz.beepro.library.model.User;
import uz.beepro.library.service.impl.UserServiceImpl;

import java.util.List;

@RestController
@RequestMapping("user")
public class UserController {
    private final UserServiceImpl userServiceImpl;
    public UserController(UserServiceImpl userServiceImpl){
        this.userServiceImpl = userServiceImpl;
    }

    @GetMapping
    public List<User> findAllUser(){
        return userServiceImpl.findAll();
    }

    @GetMapping("{id}")
    public User findUserById(@PathVariable Long id){
        return userServiceImpl.findById(id);
    }

    @PostMapping
    public User addUser(@RequestBody User user){
        return userServiceImpl.add(user);
    }

    @PutMapping("{id}")
    public User editUser(@PathVariable Long id, @RequestBody User user){
        return userServiceImpl.edit(id, user);
    }

    @DeleteMapping("{id}")
    public User deleteUser(@PathVariable Long id){
        return userServiceImpl.delete(id);
    }
}

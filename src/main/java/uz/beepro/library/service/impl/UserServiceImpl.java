package uz.beepro.library.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.beepro.library.model.User;
import uz.beepro.library.repository.UserRepository;
import uz.beepro.library.service.UserService;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;


    @Override
    public User findById(Long id) {
        Optional<User> userOptional = userRepository.findById(id);
        if(userOptional.isPresent()){
            return userOptional.get();
        }
        else return new User();
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User add(User user) {
        userRepository.save(user);
        return user;
    }

    @Override
    public User edit(Long id, User user) {
        Optional<User> userOptional = userRepository.findById(id);
        if(userOptional.isPresent()){
            User editingUser = userOptional.get();
            editingUser.setUsername(user.getUsername());
            userRepository.save(editingUser);
            return editingUser;
        }
        else return new User();
    }

    @Override
    public User delete(Long id) {
        Optional<User> userOptional = userRepository.findById(id);
        if(userOptional.isPresent()){
            User deletingUser = userOptional.get();
            userRepository.deleteById(id);
            return deletingUser;
        }
        else return new User();
    }
}

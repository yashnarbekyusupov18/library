package uz.beepro.library.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.beepro.library.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

}
